package src;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void homescreen (){

        System.out.println("Hallo, wie möchten Sie fortfahren?");

        System.out.println("1 - Gaswerte einsehen");
        System.out.println("2 - Gaswerte ändern");
        System.out.println("3 - neue Gaswerte anlegen");
        System.out.println("4 - Account erstellen");
        System.out.println("5 - Exit");
        System.out.print("Bitte geben Sie eine Zahl zwischen 1 und 5 ein: ");

        Scanner scanner = new Scanner(System.in);

        try {

            int ersteEingabe = scanner.nextInt();

            if (ersteEingabe == 1)
            {
                System.out.println("Gaswerte einsehen");
            }
            else if (ersteEingabe == 2)
            {
                System.out.println("Gaswerte ändern");
            }
            else if (ersteEingabe == 3) {
                System.out.println("neue Gaswerte anlegen");
                neueGaswerteAnlegen.verbrauchInkWh();
                homescreen();
            }
            else if (ersteEingabe == 5){
                System.out.println("Aufwiedersehen");
                System.exit(0);
            }
            else if (ersteEingabe == 4)
            {
                accountsAnlegen.anlegen();
            }
            else
            {
                System.out.println("Sie haben keine Zahl zwischen 1 und 5 eingegeben");
                homescreen();
            }

        } catch (InputMismatchException e){
            System.out.println();
            System.out.println("\033[3m" + "Sie haben ein ungültiges Zeichen eingegeben " + "\033[0m");
            System.out.println();
            homescreen();
        }
    }

    public static void main(String[] args) {

        Datenbankanbidnung.databaseConnect();
        homescreen();

    }


}