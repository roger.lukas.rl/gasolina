package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

public class accountsAnlegen {

    public static void anlegen (){

        String url = "jdbc:mysql://localhost:3306/GasRechner";
        String user = "wildau";
        String password = "wildau";

        Scanner scanner = new Scanner(System.in);

        System.out.println("Bitte Username eingeben");
        String username = scanner.next();

        try {

            System.out.println("Bitte PIN eingeben: ");
            int pin = scanner.nextInt();

            while (pin > 9999 || pin < 1000) {

                System.out.println("Bitte nur 4-Stellige PIN eingeben: ");
                pin = scanner.nextInt();

            }

            System.out.println("Bitte Postleitzahl eingeben");
            int postleitzahl = scanner.nextInt();

            while (postleitzahl > 99999 || postleitzahl < 10000) {

                System.out.println("Bitte nur 5-Stellige Postleitzahl eingeben: ");
                postleitzahl = scanner.nextInt();

            }


            try (Connection conn = DriverManager.getConnection(url, user, password)) {

                Statement stmt = conn.createStatement();
                PreparedStatement posted = conn.prepareStatement("INSERT INTO Accounts (UserName, Pin, Postleizahl) VALUES ('" + username + "', " + pin + ", " + postleitzahl + ")");

                posted.executeUpdate();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        } catch (InputMismatchException e){
            System.out.println("Unzulässiges Zeichen für PIN oder Postleitzahl");
            anlegen();
        }


    }
}
