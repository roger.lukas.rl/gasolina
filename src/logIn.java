package src;

import java.sql.*;
import java.util.Scanner;

public class logIn {

    public static String usernameAndPin (String username, int pin){

        String url = "jdbc:mysql://localhost:3306/GasRechner";
        String user = "wildau";
        String password = "wildau";

        try (Connection conn = DriverManager.getConnection(url, user, password))  {
            //System.out.println("Verbindung erfolgreich");

            String query = "SELECT ID FROM Accounts WHERE UserName = '" + username + "' AND Pin = " + pin + ";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(query);

            result.next();
            return result.getString(1);


        } catch (SQLException e) {
            System.out.println("Nutzername oder PIN Falsch eingegeben!");
            return null;
        }

    }

    public static void main (String [] args){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Bitte Username eingeben: ");
        String userName = scanner.next();

        System.out.print("Bitte PIN eingeben: ");
        int pin = scanner.nextInt();

        while(pin > 9999 || pin < 1000){
            System.out.println("PIN muss 4 Stellen haben");
            pin = scanner.nextInt();
        }

        String id = usernameAndPin(userName, pin);

        System.out.println("Deine ID ist: " + id);



    }

}
