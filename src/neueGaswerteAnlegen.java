package src;

import java.util.Scanner;

public class neueGaswerteAnlegen {

    public static void verbrauchInkWh (){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Bitte Gasvolumen angeben: ");
        int inputGasvolumen = scanner.nextInt();
        System.out.print("Bitte Zustandszahl angeben: ");
        double inputZustandszahl = scanner.nextDouble();
        System.out.print("Bitte Brennwert angeben: ");
        double inputBrennwert = scanner.nextDouble();

        double gasVerbrauchInkWh = inputGasvolumen * inputZustandszahl * inputBrennwert;

        System.out.println();
        System.out.println("\033[3m" + "Sie haben einen Verbrauch von " + gasVerbrauchInkWh + " kWh" + "\033[0m");
        System.out.println();

    }

}
